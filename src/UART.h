
#include <stdint.h>

int UART_Init(uint32_t baud);
int UART_WriteByte(uint8_t byte);
int UART_WriteBuf(uint8_t * buf,uint8_t size);
int UART_WriteString(const char * buf);
int UART_ByteReceived(uint8_t byte);
