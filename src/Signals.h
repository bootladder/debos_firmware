//
// Created by steve on 3/29/21.
//

#ifndef DEBOS_FIRMWARE_SIGNALS_H
#define DEBOS_FIRMWARE_SIGNALS_H
#include <stdint.h>
#include <stdbool.h>

#define NUMBER_OF_SIGNALS 16

typedef struct {
    int (*signalHandlerFunc) (uint8_t, uint8_t * );
    const char * name;
    const char * description;
} SignalHandler;


int Signals_SetSignalHandler(uint8_t, SignalHandler);
int Signals_CallSignalHandler(uint8_t sigNum, uint8_t * data);
SignalHandler * Signals_GetSignalHandler(uint8_t sigNum);
bool Signals_IsSignalNumberRegistered(uint8_t sigNum);
void Signals_ResetAllSignalHandlers(void);

#endif //DEBOS_FIRMWARE_SIGNALS_H
