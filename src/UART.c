#include "UART.h"
#include "Protocol.h"
#include "halUart.h"
#include "my_memcpy.h"

uint8_t uart_fifo[512];
uint16_t uart_fifo_index;


uint8_t Protocol_ReceivedNewCommand;
uint8_t Protocol_ReceivedCommandType;
uint8_t Protocol_ReceivedCommandBuffer[512];
int receiving_a_frame = 0;

int UART_ByteReceived(uint8_t byte)
{
  if( receiving_a_frame )
  {
    uart_fifo[uart_fifo_index++] = byte;
    if( uart_fifo_index ==
        (lengths_of_types[Protocol_ReceivedCommandType] - 1) )//length includes the type
                                    //but type not included in buffer
    {
      my_memcpy(Protocol_ReceivedCommandBuffer,uart_fifo, uart_fifo_index);
      receiving_a_frame = 0;
      uart_fifo_index = 0;
      Protocol_ReceivedNewCommand = 1;
      return 1;
    }
    return 2;
  }
  else if( lengths_of_types[(unsigned int)byte] == 1)
  {
    //got a single byte command
    Protocol_ReceivedNewCommand = 1;
    Protocol_ReceivedCommandType = byte;
    return 1;
  }
  else if( lengths_of_types[(unsigned int)byte] > 1)
  {
    receiving_a_frame = 1;
    Protocol_ReceivedCommandType = byte;
    return 2;
  }

  return 0;
}



int UART_WriteByte(uint8_t byte)
{
  HAL_UartWriteByte(byte);
  return 1;
}

int UART_WriteBuf(uint8_t * buf,uint8_t size)
{
  for(int i=0; i<size; i++)
  {
    HAL_UartWriteByte(buf[i]);
  }
  return 0;
}

#define MAX_UART_STRING_LENGTH 100
int UART_WriteString(const char * buf){
    for(int i=0; i<MAX_UART_STRING_LENGTH; i++){
        if(buf[i] == 0)
            return 0;
        HAL_UartWriteByte(buf[i]);
    }
    return 0;
}

int UART_Init(uint32_t baud)
{
  HAL_UartInit(baud );
  return 1;
}
