#include "Commands.h"
#include "System.h"
#include "UART.h"
#include "halFlash.h"
#include "halReset.h"
#include <stdint.h>
#include "hton.h"
#include "my_memcpy.h"
#include "halInterrupts.h"
#include "sysTypes.h"
#include "halLed.h"
#include "Signals.h"

/*
 * TODO FORCE ADDING OF ENTRY IN lengths_of_types
 */
commandHandler Handlers[16] = {

        // user -> OS
        [1] = HandlerHalt,
        [2] = HandlerReset,
        [3]= HandlerWriteFlash256,
        [4]= HandlerReadFlash256,
        [8]= HandlerListSignals,
        [9]= HandlerSendSignal,

        [15]=HandlerCheckpoint,
        // app -> OS

};

uint16_t lengths_of_types[16] = {
        [1] = 1,
        [2] = 1,
        [3] = 261,
        [4] = 5,
        [8] = 1,
        [9] = 3,
};

//App Checkpoint
int HandlerCheckpoint(uint8_t * cmd)
{
  UART_WriteByte(15); //type 15 tells the host this is a checkpoint

  uint8_t flags = cmd[4];

  //PRINT ADDRESS NETWORK ORDER
  if( flags & 0x2 )
  {
    UART_WriteByte(cmd[3]); 
    UART_WriteByte(cmd[2]);
    UART_WriteByte(cmd[1]);
    UART_WriteByte(cmd[0]);
  }

  if( flags & 0x01 )
  {
    ;//System_Active = true;
  }
  
  //PRINT MESSAGE.  ascii expected, null terminated string max 100 bytes
  if( flags & 0x08 )
  {
    uint8_t * msgbuf = (uint8_t*) & (cmd[5]);
    for(uint8_t i=0; (msgbuf[i]!=0) && (i < 100);i++)
    {
      UART_WriteByte(msgbuf[i]);
    }
    UART_WriteByte(0);//null terminator for the receive side
  }

  if( flags & 0x04 )
  {
    HAL_ResetCPU();
  }

  //LED ON
  if( flags & 0x10 )
    HAL_LedOn(0);
  else
    HAL_LedOff(0);

  return 1;
}

int HandlerReadFlash256(uint8_t * cmd)
{
  UART_WriteByte(4);
  
  uint32_t parsed_addr;
  my_memcpy(&parsed_addr,cmd,4);
  parsed_addr = ntohl(parsed_addr);
  
  if( parsed_addr & 0x000000FF || parsed_addr > 0x40000 ) {
    UART_WriteByte(2); //invalid
    return 2;
  }

  UART_WriteByte(1); //OK

  //loop through the actual flash address space here
  //flash must be accessed by 32-bit word
  uint32_t * baseptr = (uint32_t*)(uintptr_t)parsed_addr; //casting for the compiler
  static uint32_t buf[64];
  for(int i=0; i<64;i++)
  {
    buf[i] = *(baseptr + i);
  }

  uint8_t * bufbytes = (uint8_t*)buf;
  for(int i=0; i<256;i++)
  {
    UART_WriteByte(bufbytes[i]);
  }
  return 0;
}
int HandlerWriteFlash256(uint8_t* cmd)
{
  UART_WriteByte(3);
  
  uint32_t parsed_addr;
  my_memcpy(&parsed_addr,cmd,4);
  parsed_addr = ntohl(parsed_addr);

  if( parsed_addr & 0x000000FF ) {
    UART_WriteByte(2); //invalid
    return 2;
  }
HAL_DisableInterruptsGlobal();
  int ret = Commands_WriteFlash256(parsed_addr, &(cmd[4]) );
HAL_EnableInterruptsGlobal();
  if( ret == 1 )
    UART_WriteByte(1); //success
  else
    UART_WriteByte(3); //fail
    
  return 0;
}

int HandlerReset(uint8_t* cmd)
{
  HAL_ResetCPU();
  return 1;
  (void)cmd;
}
int HandlerHalt( uint8_t* cmd)
{
  UART_WriteByte(1); //fail
  System_AppHalted = 1;
//NVIC_DisableIRQ(RTC_IRQn);
//NVIC_DisableIRQ(EIC_IRQn);
//uint32_t irqs = NVIC->ISER[0];
NVIC->ICER[0] = 0xFFFFFFFF;
  NVIC_EnableIRQ(SERCOM4_IRQn);
  return 1;
  (void)cmd;
}

/*
 * Parameter ignored
 * Just print out the signals to UART
 * in a human readable format
 */
int HandlerListSignals(uint8_t * cmd){
    (void)cmd;

    UART_WriteString("Listing Signals: \n");
    bool foundASignal = 0;
    for(int i=0; i<NUMBER_OF_SIGNALS; i++){
        if(Signals_IsSignalNumberRegistered(i)){
            foundASignal = true;
            SignalHandler * ptrHandler = Signals_GetSignalHandler(i);

            UART_WriteString("Signal Number: ");
            UART_WriteByte('0' + i);
            UART_WriteByte('\n');
            UART_WriteString("Name: ");
            UART_WriteString(ptrHandler->name);
            UART_WriteByte('\n');
            UART_WriteString("Description: ");
            UART_WriteString(ptrHandler->description);
            UART_WriteByte('\n');
            UART_WriteByte('\n');
        }

    }

    if(false == foundASignal){
        UART_WriteString("No Signal Handlers Registered. \n");
        return 0;
    }
    return 1;
}

int HandlerSendSignal(uint8_t * cmd) {
    UART_WriteString("Calling Signal Handler: ");

    uint8_t sigNum = cmd[0];
    uint8_t * sigArg = &cmd[1];

    UART_WriteByte('0'+sigNum);
    UART_WriteString(" , ");
    UART_WriteByte(*sigArg);
    UART_WriteByte('\n');


    Signals_CallSignalHandler(sigNum, sigArg);
    return 0;
}

////////////////

int Commands_CommandReceived(int type,uint8_t* cmd)
{
  if( Handlers[type]){
    Handlers[type](cmd);
    return 1;
  }
  return 0;
}

int Commands_WriteFlash256(uint32_t addr, uint8_t* buf)
{
  if( !addr || !buf )
    return 2;

  uintptr_t checker = (uintptr_t) addr;
  checker&=0xFF;
  if( checker )
    return 2;

  if( Flash_Write256(addr,buf) == 1 )
    return 1;
  else
    return 3;
}


///////////////////////////
// for testing
void Commands_SetHandlerFunc(int cmdNum, commandHandler handler){
    Handlers[cmdNum] = handler;
}