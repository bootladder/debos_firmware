#include "halLed.h"
#include "halDelay.h"
#include "UART.h"
#include "halUart.h"
#include "System.h"
#include "sysTypes.h"
#include "halInterrupts.h"
#include "clocks.h"

#ifndef MAJOR_VERSION
  #define MAJOR_VERSION 0
#endif
#ifndef MINOR_VERSION
  #define MINOR_VERSION 0
#endif
#ifndef BUILD_VERSION
  #define BUILD_VERSION 0
#endif
int appexists = 0;


int main(void)
{
  HAL_LedInit();


clocks_init();

  //For the Flash functions
  PM->AHBMASK.reg |= PM_AHBMASK_NVMCTRL;
  PM->APBBMASK.reg |= PM_APBBMASK_NVMCTRL;

  HAL_EnableInterruptsGlobal();
  HAL_UartInit(115200);

  //indicate power cycle
	//the LED should be on for on the order of 1 second.
  HAL_DelayLoops(50000);HAL_LedOn(0);
  HAL_DelayLoops(10000000);HAL_LedOff(0);

  uint8_t hellomsg[20] = "DebOS Firmware: ";
  uint8_t buildstring[10] = "0.0.0";
  buildstring[0] = '0'+MAJOR_VERSION;
  buildstring[2] = '0'+MINOR_VERSION;
  buildstring[4] = '0'+BUILD_VERSION;
  UART_WriteBuf(hellomsg,sizeof(hellomsg));
  UART_WriteBuf(buildstring,sizeof(buildstring));

  HAL_UartWriteByte('\n');

  System_SetApplicationHeaderAddress((void*)0x2100);
  if(System_DetectApplication())
  {
    System_Active = 0;
    appexists  = 1;
    UART_WriteBuf((uint8_t *)"Running App...\n",15);
    HAL_LedOn(0);
    HAL_DelayLoops(400000);HAL_LedOff(0);
  }
  else //no app
  {
    UART_WriteBuf((uint8_t *)"No App Found..\n",15);
   HAL_DelayLoops(100000);HAL_LedOn(0);
   HAL_DelayLoops(100000);HAL_LedOff(0);
   HAL_DelayLoops(100000);HAL_LedOn(0);
   HAL_DelayLoops(100000);HAL_LedOff(0);
   System_Active = 1;
  }

  //transmit the UART message before starting the app
  for(uint8_t i=0;i<250;i++)
  {
   HAL_DelayLoops(1000);HAL_UartTaskHandler();
  }

    //for test just let the system always run
    System_Active = 1;

  System_ClearAppRAM();


#define TESTING
#ifdef TESTING
	#include "tests.h"
	tests_run();
#endif

  int times  = 1; //run the app this many times (test)
  while(1)
  {
    HAL_UartTaskHandler();

    if( System_Active )
      System_RunSystem();

    if( appexists && times && !System_AppHalted) {
      System_RunApplication();
      //times = 0;
    }
  }


}
