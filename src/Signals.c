//
// Created by steve on 3/29/21.
//

#include <string.h>
#include "Signals.h"

SignalHandler signalHandlers[16];


int Signals_SetSignalHandler(uint8_t sigNum, SignalHandler handler) {
    signalHandlers[sigNum] = handler;
    return 0;
}

int Signals_CallSignalHandler(uint8_t sigNum, uint8_t * data){
    signalHandlers[sigNum].signalHandlerFunc(sigNum, data);
    return 0;
}

SignalHandler * Signals_GetSignalHandler(uint8_t sigNum){
    return &(signalHandlers[sigNum]);
}

bool Signals_IsSignalNumberRegistered(uint8_t sigNum){
    if(signalHandlers[sigNum].signalHandlerFunc != 0)
        return true;
    return false;
}

void Signals_ResetAllSignalHandlers(void){
    memset(signalHandlers, 0, sizeof(signalHandlers));
}