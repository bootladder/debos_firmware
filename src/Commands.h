#include <stdint.h>

int HandlerCheckpoint(uint8_t * cmd);
int HandlerReadFlash256(uint8_t * cmd);
int HandlerWriteFlash256(uint8_t * cmd);
int HandlerHalt(uint8_t  * cmd);
int HandlerReset(uint8_t * cmd);
int HandlerListSignals(uint8_t * cmd);
int HandlerSendSignal(uint8_t * cmd);

int Commands_CommandReceived(int type,uint8_t * cmd);
int Commands_WriteFlash256(uint32_t addr, uint8_t * buf);

// for testing
typedef int (*commandHandler)(uint8_t * cmd);
void Commands_SetHandlerFunc(int, commandHandler);