
void HAL_IrqHandlerRtc(void);
void HAL_IrqHandlerTc0(void);
void HAL_IrqHandlerTc1(void);
void HAL_IrqHandlerTc2(void);
void HAL_IrqHandlerTc3(void);
void HAL_IrqHandlerTc4(void);
void HAL_IrqHandlerTc5(void);
void HAL_IrqHandlerTc6(void);
void HAL_IrqHandlerAdc(void);
void HAL_IrqHandlerEic(void);
void HAL_IrqHandlerSercom0(void);
