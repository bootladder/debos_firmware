#ifndef _HAL_LED_H_
#define _HAL_LED_H_
void HAL_LedInit(void);
void HAL_LedClose(void);
void HAL_LedOn(int i);
void HAL_LedOff(int i);
void HAL_LedToggle(int i);
#endif // _HAL_LED_H_
