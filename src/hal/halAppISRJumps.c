/***VECTORS TO APP VECTORS************************************************//**
***********CALL THE FUNCTION AT ADDRESS IN TABLE INDEX***********************/

#include "halAppISRJumps.h"
#include "sysTypes.h"
#include "System.h"
#include "halDelay.h"
#include "halLed.h"


typedef void (*myfptr)(void);

//myfptr indexpointer;
//static uint8_t checkpointbuf[20];

void HAL_IrqHandlerEic(void)
{
}
void HAL_IrqHandlerSercom0(void)
{
    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*4));
    uint32_t ival = *i;
    ((myfptr)ival)();
}
void HAL_IrqHandlerTc0(void)
{
    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*10));
    uint32_t ival = *i;
    ((myfptr)ival)();
}
void HAL_IrqHandlerTc1(void)
{
    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*11));
    uint32_t ival = *i;
    ((myfptr)ival)();
}
void HAL_IrqHandlerTc2(void)
{
    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*12));
    uint32_t ival = *i;
    ((myfptr)ival)();
}
void HAL_IrqHandlerTc3(void)
{
    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*13));
    uint32_t ival = *i;
    ((myfptr)ival)();
}
void HAL_IrqHandlerTc4(void)
{
    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*14));
    uint32_t ival = *i;
    ((myfptr)ival)();
}
void HAL_IrqHandlerTc5(void)
{
    if(System_AppHalted)
        return  ;
    uint32_t * i = (uint32_t *)(0x204C+(4*15));
    uint32_t ival = *i;
    ((myfptr)ival)();
}
void HAL_IrqHandlerTc6(void)
{
    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*16));
    uint32_t ival = *i;
    ((myfptr)ival)();
}



void HAL_IrqHandlerAdc(void)
{
    while(1){;HAL_DelayLoops(100000);HAL_LedToggle(0);   }

    if(System_AppHalted)
        return;
    uint32_t * i = (uint32_t *)(0x204C+(4*18));
    uint32_t ival = *i;
    ((myfptr)ival)();
}



void HAL_IrqHandlerRtc(void)
{
//while(1){;HAL_DelayLoops(100000);HAL_LedToggle(0);   }
		//RTC->MODE0.INTFLAG.reg = 0xFF;
if(System_AppHalted)
  return;
//while(1){;HAL_DelayLoops(100000);HAL_LedOn(0);   }
    //HAL_LedToggle(0);
//while(1){;}
  uint32_t * i = (uint32_t *)0x204C;
  uint32_t ival = *i;
  ((myfptr)ival)();
}



//my_memcpy(checkpointbuf,"ival:           \n\0",18);
//  _1byte_to_2asciihexbytes(  ival&0x000000FF, &checkpointbuf[5]);
//  _1byte_to_2asciihexbytes( (ival&0x0000FF00)>>8, &checkpointbuf[7]);
//  _1byte_to_2asciihexbytes( (ival&0x00FF0000)>>16, &checkpointbuf[9]);
//  _1byte_to_2asciihexbytes( (ival&0xFF000000)>>24, &checkpointbuf[11]);
//  checkpointbuf[6] = ival&0xFF;
//  checkpointbuf[5] = 
 //System_Checkpoint(0x3030, 0x8|0x2,checkpointbuf );

//static void _1byte_to_2asciihexbytes(uint8_t onebyte, uint8_t onebyteformatted[2])
//{
//    onebyteformatted[0] = onebyte >> 4;
//    if(onebyteformatted[0] < 10)
//      onebyteformatted[0] = '0'+onebyteformatted[0];
//    else
//      onebyteformatted[0] = 'A'+(onebyteformatted[0]-10);
//
//    onebyteformatted[1] = onebyte  & 0x0F;
//    if(onebyteformatted[1] < 10)
//     onebyteformatted[1] = '0'+onebyteformatted[1];
//    else
//      onebyteformatted[1] = 'A'+(onebyteformatted[1]-10);
//}
