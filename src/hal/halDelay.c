#include "halDelay.h"
#include <stdint.h>

//checking the ELF file, the loop body takes
// cmp beq add add b, so 5 instructions
int HAL_DelayLoops(uint32_t loops)
{
  int z=0;
  for(uint32_t i=0;i<loops;i++)
   z+=i; 
  if(z > 100)
    return 1;
  return 0;
}
