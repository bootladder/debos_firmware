#include "halLed.h"
#include "halGpio.h"

HAL_GPIO_PIN(LED0, A, 14);

void HAL_LedInit(void)
{
  HAL_GPIO_LED0_out();
  HAL_GPIO_LED0_set();
}

void HAL_LedClose(void)
{
  HAL_GPIO_LED0_in();
}

void HAL_LedOn(int i)
{
  if (0 == i)
    HAL_GPIO_LED0_clr();
}

void HAL_LedOff(int i)
{
  if (0 == i)
    HAL_GPIO_LED0_set();
}

void HAL_LedToggle(int i)
{
  if (0 == i)
    HAL_GPIO_LED0_toggle();
}
