#ifndef _HAL_INTERRUPTSHAL_
#define _HAL_INTERRUPTSHAL_
void HAL_EnableInterruptsGlobal(void);
void HAL_DisableInterruptsGlobal(void);
#endif
