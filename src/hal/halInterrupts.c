#include "halInterrupts.h"

void HAL_EnableInterruptsGlobal(void)
{
  __asm volatile ("cpsie i"); //enable interrupts
}
void HAL_DisableInterruptsGlobal(void)
{
  __asm volatile ("cpsid i"); //disableinterrupts
}
