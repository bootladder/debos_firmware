#include "System.h"
#include "Protocol.h"
#include "Commands.h"
#include "Signals.h"
#include "hton.h"
#include <string.h>
#include "halDelay.h"
#include "halLed.h"

volatile int System_Active;
volatile int System_AppHalted;

//This is the pointer to the application, that the system calls
void * applicationHeaderAddr;

typedef struct {
    int (*System_Checkpoint_Pointer)(uint32_t,uint8_t,uint8_t *);
    int (*System_SetSignalHandler_Pointer)(uint8_t,SignalHandler);
} SystemCallTable;

CHECKPOINTPOINTER_SECTION
SystemCallTable systemCallTable = {
        &System_Checkpoint,
        &Signals_SetSignalHandler
};

//this is the pointer that the App calls
;

//int (*System_Checkpoint_Pointer)(uint32_t,uint8_t,uint8_t *) = & System_Checkpoint;
//
//CHECKPOINTPOINTER_SECTION
//int (*System_SetSignalHandler_Pointer)(uint8_t,SignalHandler) = & Signals_SetSignalHandler;
//

int System_SetApplicationHeaderAddress(void* addr)
{
  applicationHeaderAddr = addr;
  return 0;
}

int System_DetectApplication(void)
{
  ApplicationHeader_t * app = 
        (ApplicationHeader_t *) applicationHeaderAddr; 

  //Check that app pointer is there,and points to flash above 0x1000
  uintptr_t startptr = (uintptr_t) app->application_start;

  if( startptr == 0)
    return 0;
  if( startptr < 0x1000)
    return 0;
  if( startptr > 0x30000)
    return 0;

  return 1;
}


void System_ClearAppRAM(void)
{
  uint32_t * start=(uint32_t * )0x20002000;
  for(int i=0; i < (0x6000/4);i++)
  {
    start[i] = 0;
  }
}

typedef void (funcptr)(void);

int System_RunApplication(void)
{
  //first dereference the address
  uint32_t * addrptr = (uint32_t*) 0x2100;
  uint32_t addr = *addrptr ;
  
    funcptr* apprunner = (funcptr *)(uintptr_t) addr ;

  if(apprunner)
  {
    //if( //address check for test purposes
    //    (apprunner) == 0x2100 ||
    //    (apprunner) == 0x2101 
    //)
      (apprunner)();
  }
  else{ //app = 0, fatal
    while(1)  {
      HAL_DelayLoops(1000000);HAL_LedOn(0);
      HAL_DelayLoops(1000000);HAL_LedOff(0);
    }
  }
  return 1;
}

int System_RunSystem(void)
{ 
  //if( !System_Checkpoint_Pointer ) //make compiler happy
  //  return 9; 
  if( System_Active == 0 )
    return 0;
  else
  {
    if( Protocol_ReceivedNewCommand )
    {
      Commands_CommandReceived( 
          Protocol_ReceivedCommandType, 
          Protocol_ReceivedCommandBuffer);
      Protocol_ReceivedNewCommand = 0;
      return 1;
    }
  }

  return 0;
}

int System_Checkpoint(uint32_t addr, uint8_t flags, uint8_t * msg)
{
  (void)addr;
  Protocol_ReceivedNewCommand = 1;
  Protocol_ReceivedCommandType = 15;//checkpoint
  
  //get the return address from this function
  void * newaddr = __builtin_return_address(0);
  uintptr_t newptr = (uintptr_t)newaddr;
  //put address in the command
  //addr = htonl(addr);
  //memcpy(Protocol_ReceivedCommandBuffer,&addr,4); 
  newptr = htonl(newptr);
  memcpy(Protocol_ReceivedCommandBuffer,&newptr,4); 

  //put flags in the command
  Protocol_ReceivedCommandBuffer[4] = flags;

  //copy the message
  uint8_t * start = &(Protocol_ReceivedCommandBuffer[5]);
  uint8_t i=0;
  for(; msg[i] != 0; i++){
   start[i] = msg[i]; 
   if(i >= 100 ){
      start[0] = 0; //too long message. ignore
      break;
   }
  }
  start[i] = 0;//null terminator
  return 1;
}

/*
static int System_CalculateChecksum(void)
{
  ApplicationHeader_t * app = 
    (ApplicationHeader_t *) applicationHeaderAddr; 

  unsigned int checksum = 0;
  uint32_t temptrav = 0;
  uint32_t temprotatetrav = 0;
  uint32_t * trav = app->application_start;
  for(unsigned int i=0;i<(app->length)/4;i++)
  {
    temprotatetrav = trav[i];
    for(int i=0;i<4;i++){
      temptrav = temprotatetrav & 0xFF;
      checksum+=temptrav; 
      temprotatetrav>>=8; 
    }
  }
  if( checksum == app->checksum)
    return 1;
  else
    return 2;//bad checksum
}
*/
