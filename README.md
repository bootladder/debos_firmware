# "Debug OS" .   #
It's a bootloader that starts your application and then allows you to debug your application by giving it a couple hooks to call, at hard coded addresses. 

Your app does not supply main(), it supplies the while(1){} of your application, but without the while(1).  The system will call it in it's own while(1){}.
Your app binary is the complete flash address space, with your app function starting at the predefined app start address.

Use the DebOS Host Client to load your app to the target.  The Host Client knows how to seek through the binary image to get your app.  I found it more convenient to get the app to compile with a dummy main() and have the vector table in the image etc. all that is ignored by the system.

# "Checkpoints:  The new breakpoint.  It's like the opposite of an assert #

A checkpoint is like an assert but you write a passing condition, and it traps to the system when it is reached.  The system can either continue running the application or halt, allowing commands to be issued eg. read memory at address, read register value etc.
You put checkpoints in at compile time, and you also put in "checkpoint handlers" to tell the system what to do when a checkpoint is reached.

The main purpose of a checkpoint is to print the address of the call to the checkpoint.  The Host can then use that address to figure out where in the source the condition is.  (addr2line)

You can also tell the system to do stuff via the checkpoint interface like print an optional message, turn on the system LED, halt etc.

# Asserts of the traditional kind will be supported in some way, not sure yet.

# Check out the Demo!  A 802.15.4 sniffer using the Atmel 212B tranceiver and a SAMD20 XPlained Pro dev kit.
