#include "unity.h"
#include "unity_fixture.h"

#include "Commands.h"
#include "Signals.h"
#include "MockhalFlash.h"
#include "MockhalUart.h"
#include <string.h>
#include <stdint.h>
#include "hton.h"

TEST_GROUP_RUNNER(Commands)
{
    RUN_TEST_CASE(Commands, CommandReceived_Invalid_Returns0);
    RUN_TEST_CASE(Commands, CommandReceived_Valid_CallsHandlerReturns1);

    RUN_TEST_CASE(Commands, HandlerWriteFlash256_ValidCommandFlashOK_SendsResponseUART);
    RUN_TEST_CASE(Commands, HandlerWriteFlash256_ValidCommandFlashBAD_SendsResponseUART);
    RUN_TEST_CASE(Commands, HandlerWriteFlash256_InvalidCommand_SendsResponseUART);

    RUN_TEST_CASE(Commands, WriteFlash256_ValidInput_FlashOK_Returns1);
    RUN_TEST_CASE(Commands, WriteFlash256_InvalidInput_Returns2);
    RUN_TEST_CASE(Commands, WriteFlash256_ValidInput_FlashBAD_Returns3);

    RUN_TEST_CASE(Commands, ListSignals_NoSignals_Returns0);
    RUN_TEST_CASE(Commands, ListSignals_OneSignal_Returns1);
    RUN_TEST_CASE(Commands, ListSignals_OneSignal_WritesNameAndDescription_toUart);

    RUN_TEST_CASE(Commands, SendSignal_CallsSignalHandlerWithArgument);
}




//These are usually driven by UART driver.  we can fake it here
unsigned int Protocol_ReceivedNewCommand;
unsigned int Protocol_ReceivedCommandType;
uint8_t Protocol_ReceivedCommandBuffer[512];

static int dummyCommandHandler(uint8_t * cmd){
    (void)cmd;
    return 1;
}

static int dummySignalHandler(uint8_t sigNum, uint8_t * sigData) {
    (void)sigNum; (void)sigData;
    return 0;
}


TEST_GROUP(Commands);

TEST_SETUP(Commands)
{
  MockhalUart_Init();
  Signals_ResetAllSignalHandlers();
}

TEST_TEAR_DOWN(Commands)
{
}

TEST(Commands, CommandReceived_Invalid_Returns0 )
{
  uint8_t a[2] = "a";
  uint8_t * dummy = 0;
  TEST_ASSERT_EQUAL(0, Commands_CommandReceived(a[0],dummy));
}
TEST(Commands, CommandReceived_Valid_CallsHandlerReturns1)
{
    Commands_SetHandlerFunc(1, dummyCommandHandler);

    uint8_t a[2];
    a[0] = 1;
    uint8_t * dummy = 0;
    TEST_ASSERT_EQUAL(1, Commands_CommandReceived(a[0],dummy));
}

TEST(Commands, HandlerWriteFlash256_ValidCommandFlashOK_SendsResponseUART)
{
  uint8_t buf[260];
  uintptr_t addr = 0x3000;
  addr = htonl(addr);
  memcpy(buf,&addr,4);
  uint8_t a[1];
  a[0] = 3;
  MockFlash_SetToSuccess();
  Commands_CommandReceived(a[0],buf);
  TEST_ASSERT_MESSAGE(3 == MockhalUart_GetByteAtIndex(0) , "First byte not equal to type = 3");
  TEST_ASSERT_MESSAGE(1 == MockhalUart_GetByteAtIndex(1) , "2nd byte not equal to success = 1");
}

TEST(Commands, HandlerWriteFlash256_ValidCommandFlashBAD_SendsResponseUART)
{
  uint8_t buf[260];
  uintptr_t addr = 0x3000;
  addr = htonl(addr);
  memcpy(buf,&addr,4);
  uint8_t a[1];
  a[0] = 3;
  MockFlash_SetToFail();
  Commands_CommandReceived(a[0],buf);
  TEST_ASSERT_EQUAL_INT_MESSAGE(3, MockhalUart_GetByteAtIndex(0) , "1st byte not equal to type = 3");
  TEST_ASSERT_EQUAL_INT_MESSAGE(3, MockhalUart_GetByteAtIndex(1) , "2nd byte not equal to fail = 3");
}

TEST(Commands, HandlerWriteFlash256_InvalidCommand_SendsResponseUART)
{
  uint8_t buf[260];
  uintptr_t addr = 0x3001;
  addr = htonl(addr);
  memcpy(buf,&addr,4);
  uint8_t a[1];
  a[0] = 3;
  MockFlash_SetToSuccess(); //
  Commands_CommandReceived(a[0],buf);
  TEST_ASSERT_EQUAL_INT_MESSAGE(3, MockhalUart_GetByteAtIndex(0) , "1st byte != type = 3");
  TEST_ASSERT_EQUAL_INT_MESSAGE(2, MockhalUart_GetByteAtIndex(1) , "2nd byte != invalid = 2");
}

TEST(Commands, WriteFlash256_ValidInput_FlashOK_Returns1)
{
  uint8_t buf[256];
  uint32_t addr = 0x3000;
  MockFlash_SetToSuccess();
  TEST_ASSERT_EQUAL(1, Commands_WriteFlash256(addr,buf));
}

TEST(Commands, WriteFlash256_InvalidInput_Returns2)
{
  //All of these should pass
  uint8_t buf[256];
  uint32_t addr = 0x3001;
  TEST_ASSERT_EQUAL(2, Commands_WriteFlash256(addr,buf));
}

TEST(Commands, WriteFlash256_ValidInput_FlashBAD_Returns3)
{
  MockFlash_SetToFail();
  //All of these should pass
  uint8_t buf[256];
  uint32_t addr = 0x3000;
  TEST_ASSERT_EQUAL(3, Commands_WriteFlash256(addr,buf));
}

TEST(Commands, ListSignals_NoSignals_Returns0)
{
    int ret = HandlerListSignals((uint8_t *) 0);
    TEST_ASSERT_EQUAL(ret, 0);
}

TEST(Commands, ListSignals_OneSignal_Returns1)
{
    SignalHandler handler;
    handler.signalHandlerFunc = dummySignalHandler;
    Signals_SetSignalHandler(1, handler);
    int ret = HandlerListSignals((uint8_t *) 0);
    TEST_ASSERT_EQUAL(ret, 1);
}

TEST(Commands, ListSignals_OneSignal_WritesNameAndDescription_toUart)
{
    SignalHandler handler;
    handler.signalHandlerFunc = dummySignalHandler;
    handler.name = "name";
    handler.description = "description";
    Signals_SetSignalHandler(1, handler);

    HandlerListSignals((uint8_t *) 0);

    TEST_ASSERT_TRUE(strstr((const char *)MockhalUart_GetEntireBuffer(), handler.name));
    TEST_ASSERT_TRUE(strstr((const char *)MockhalUart_GetEntireBuffer(), handler.description));
}

static uint8_t mockSigNum = 0;
static uint8_t * mockSigArg = 0;
static int mockSignalHandlerFunc(uint8_t sigNum, uint8_t * sigArg){
    mockSigNum = sigNum;
    mockSigArg = sigArg;
    return 0;
}

TEST(Commands, SendSignal_CallsSignalHandlerWithArgument)
{
    SignalHandler handler;
    handler.signalHandlerFunc = mockSignalHandlerFunc;
    Signals_SetSignalHandler(1, handler);

    uint8_t buf[100];
    buf[0] = 1;
    buf[1] = 'Z';

    Commands_CommandReceived(9, buf);

    TEST_ASSERT_EQUAL(mockSigNum, 1);
    TEST_ASSERT_EQUAL(mockSigArg[0], 'Z');
}
