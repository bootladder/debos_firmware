#include "unity.h"
#include "unity_fixture.h"

#include "UART.h"
#include "Protocol.h"
#include "string.h"

TEST_GROUP_RUNNER(UART)
{
    RUN_TEST_CASE(UART, ByteReceived_TypeLength1_CallsHandlerReturns1);
    RUN_TEST_CASE(UART, ByteReceived_InvalidType_DoesNothingReturns0);
    RUN_TEST_CASE(UART, ByteReceived_FlashWrite256Command_ReceivedProperly);
    RUN_TEST_CASE(UART, BufferFinishedConsuming_ResetsIndex);
    //RUN_TEST_CASE(UART, ConsumeBuffer_3bytes_);
}


extern unsigned int uart_fifo_index;
extern uint8_t uart_fifo[100];

TEST_GROUP(UART);

TEST_SETUP(UART)
{
  uart_fifo_index = 0;
  memset(uart_fifo,0,100);
  Protocol_ReceivedNewCommand = 0;
  Protocol_ReceivedCommandType = 0;
}

TEST_TEAR_DOWN(UART)
{
}

TEST(UART, ByteReceived_TypeLength1_CallsHandlerReturns1)
{
  uint8_t a = 1;
  TEST_ASSERT_EQUAL(1,UART_ByteReceived(a));
  TEST_ASSERT_EQUAL(1,Protocol_ReceivedNewCommand);
  TEST_ASSERT_EQUAL(1,Protocol_ReceivedCommandType);
}

TEST(UART, ByteReceived_InvalidType_DoesNothingReturns0)
{
  uint8_t a = 37;
  TEST_ASSERT_EQUAL(0,UART_ByteReceived(a));
  TEST_ASSERT_EQUAL(0,Protocol_ReceivedNewCommand);
}

TEST(UART, ByteReceived_FlashWrite256Command_ReceivedProperly)
{
  TEST_ASSERT_EQUAL_INT_MESSAGE(2,UART_ByteReceived(3),"First type byte not received");

  uint8_t * dummything = (uint8_t *)0x0300;
  uintptr_t pointertodummy = (uintptr_t)dummything;
  uint8_t addressofdummy = 0;
  for(int i=0;i<4;i++)
  {
    addressofdummy = pointertodummy&0xFF;
    TEST_ASSERT_EQUAL(2,UART_ByteReceived(addressofdummy));
    pointertodummy >>= 8;
  }
  TEST_ASSERT_EQUAL(4,uart_fifo_index);

  //char flashbuf[256];
  for(int i=0;i<255;i++) //write 255
    TEST_ASSERT_EQUAL(2,UART_ByteReceived(i)); 
  TEST_ASSERT_EQUAL(1,UART_ByteReceived(1));//write last one

  TEST_ASSERT_EQUAL(1,Protocol_ReceivedNewCommand);
  TEST_ASSERT_EQUAL(3,Protocol_ReceivedCommandType);
}

TEST(UART, BufferFinishedConsuming_ResetsIndex)
{
}


