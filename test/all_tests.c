#include "unity_fixture.h"

static void RunAllTests(void)
{
  RUN_TEST_GROUP(Commands);
  RUN_TEST_GROUP(Signals);
  RUN_TEST_GROUP(System);
  RUN_TEST_GROUP(UART);
}

int main(int argc, const char * argv[])
{
  return UnityMain(argc, argv, RunAllTests);
}
