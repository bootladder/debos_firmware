#include "unity.h"
#include "unity_fixture.h"

#include "System.h"
#include "Protocol.h"
#include <stdint.h>


TEST_GROUP_RUNNER(System)
{
    RUN_TEST_CASE(System, DetectApplication_Exists_Returns1);
    RUN_TEST_CASE(System, DetectApplication_NotExists_Returns0);
    RUN_TEST_CASE(System, RunSystem_ActiveAndCommandReceived_RunsCommand);
    RUN_TEST_CASE(System, DetectApplication_BadChecksum_Returns2);
    RUN_TEST_CASE(System, RunApplication_Returns2_SetHaltFlag);
}


ApplicationHeader_t TestHeader;
uint8_t app[256];
extern volatile int System_Active;

TEST_GROUP(System);

TEST_SETUP(System)
{
  //put a legit application in memory
  for(int i=0;i<256;i++){
    app[i] = 0;
  }
  
  TestHeader.application_start = (void *)0x1100;
  TestHeader.length = 256;
  TestHeader.checksum = 0;
  (void)lengths_of_types;
}

TEST_TEAR_DOWN(System)
{
}

TEST(System,DetectApplication_Exists_Returns1)
{
  System_SetApplicationHeaderAddress(&TestHeader);
  TEST_ASSERT_EQUAL(1,System_DetectApplication());
}

IGNORE_TEST(System,DetectApplication_BadChecksum_Returns2)
{
  //put a legit application in memory
  uint8_t app[256];
  for(int i=0;i<256;i++){
    app[i] = 7;
  }
  TestHeader.application_start = app;
  TestHeader.length = 256;
  TestHeader.checksum = 9;
  

  System_SetApplicationHeaderAddress(&TestHeader);
  TEST_ASSERT_EQUAL(2,System_DetectApplication());
}

TEST(System,RunSystem_ActiveAndCommandReceived_RunsCommand)
{
  System_Active = 1;
  //set up a received command
  for(int i=0;i<260;i++)
  {
    Protocol_ReceivedCommandBuffer[i] = i;
  }
  Protocol_ReceivedCommandType = 3;
  Protocol_ReceivedNewCommand = 1;
  

  TEST_ASSERT_EQUAL(1,System_RunSystem());
};

TEST(System,DetectApplication_NotExists_Returns0)
{

};

TEST(System,RunApplication_Returns2_SetHaltFlag)
{

};


TEST(System,Checkpoint_CreatesCheckpointCommand)
{
  System_Checkpoint(0x3000,0,0);
  TEST_ASSERT_EQUAL_INT_MESSAGE(16,Protocol_ReceivedCommandType,"wrong type 16");
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,Protocol_ReceivedNewCommand,"command new not set");
};

