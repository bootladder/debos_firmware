#include "MockhalFlash.h"

static int setToSuccess = 0;

void MockFlash_SetToSuccess(void)
{
  setToSuccess = 1;
}

void MockFlash_SetToFail(void)
{
  setToSuccess = 0;
}
int Flash_Write256(uint8_t * addr, uint8_t * buf)
{
  if(setToSuccess)
    return 1;
  else
    return 0; 
  (void)addr;(void)buf;
}
