#include "MockhalUart.h"
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#define MOCKBUF_LENGTH 1024
uint8_t mockbuf[MOCKBUF_LENGTH];
int mockindex = 0;

void HAL_UartWriteByte(uint8_t byte)
{
  mockbuf[mockindex++] = byte;
}
uint8_t MockhalUart_GetByteAtIndex(int index)
{
  return mockbuf[index];
}

uint8_t * MockhalUart_GetEntireBuffer(void){
    return mockbuf;
}

void MockhalUart_Init(void)
{
   memset(mockbuf,0,MOCKBUF_LENGTH);
    mockindex = 0;
}

void HAL_UartInit(uint32_t baudrate)
{
  (void)baudrate;
  return;
}
