#ifndef __MOCKUART_H_
#define __MOCKUART_H_
#include <stdint.h>
void HAL_UartWriteByte(uint8_t byte);
void HAL_UartInit(uint32_t baudrate);
uint8_t MockhalUart_GetByteAtIndex(int index);
uint8_t * MockhalUart_GetEntireBuffer(void);
void MockhalUart_Init(void);

#endif
