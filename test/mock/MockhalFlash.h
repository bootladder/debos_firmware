#include <stdint.h>

int Flash_Write256(uint8_t * addr, uint8_t * buf);
void MockFlash_SetToSuccess(void);
void MockFlash_SetToFail(void);
