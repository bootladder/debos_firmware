#include "unity.h"
#include "unity_fixture.h"

#include "Signals.h"

TEST_GROUP_RUNNER(Signals)
{
    RUN_TEST_CASE(Signals, Set_SignalHandler_thenCall_handlerIsCalled);
    RUN_TEST_CASE(Signals, Set_SignalHandlers_thenCallBoth);
    RUN_TEST_CASE(Signals, Set_SignalHandler_AddedDescriptorFields);
}



int mockSignalHandler_called = 0;
static int mockSignalHandler(uint8_t sigNum, uint8_t * sigData) {
    (void)sigNum; (void)sigData;
    mockSignalHandler_called = 1;
    return 0;
}

int mockOtherSignalHandler_called = 0;
static int mockOtherSignalHandler(uint8_t sigNum, uint8_t * sigData) {
    (void)sigNum; (void)sigData;
    mockOtherSignalHandler_called = 1;
    return 0;
}


TEST_GROUP(Signals);

TEST_SETUP(Signals)
{
    mockSignalHandler_called = 0;
}

TEST_TEAR_DOWN(Signals)
{
}

TEST(Signals, Set_SignalHandler_thenCall_handlerIsCalled)
{
    SignalHandler handler;
    handler.signalHandlerFunc = mockSignalHandler;
    Signals_SetSignalHandler(1, handler);
    Signals_CallSignalHandler(1, (uint8_t *) 0);

    TEST_ASSERT_EQUAL(mockSignalHandler_called, 1);
}

TEST(Signals, Set_SignalHandlers_thenCallBoth)
{
    SignalHandler handler1;
    handler1.signalHandlerFunc = mockSignalHandler;
    SignalHandler handler2;
    handler2.signalHandlerFunc = mockOtherSignalHandler;

    Signals_SetSignalHandler(1, handler1);
    Signals_SetSignalHandler(2, handler2);

    Signals_CallSignalHandler(1, (uint8_t *) 0);
    Signals_CallSignalHandler(2, (uint8_t *) 0);

    TEST_ASSERT_EQUAL(mockSignalHandler_called,1);
    TEST_ASSERT_EQUAL(mockOtherSignalHandler_called,1);
}

TEST(Signals, Set_SignalHandler_AddedDescriptorFields)
{
    SignalHandler handler;
    handler.signalHandlerFunc = mockSignalHandler;
    handler.name = "Handler One";
    handler.description = "Description of Handler One";

    Signals_SetSignalHandler(1, handler);

    SignalHandler * ptrHandler = Signals_GetSignalHandler(1);
    TEST_ASSERT_EQUAL_STRING(ptrHandler->name, "Handler One");
    TEST_ASSERT_EQUAL_STRING(ptrHandler->description, "Description of Handler One");
}
